import { BrowserRouter, Routes, Route } from "react-router-dom";
import PegawaiList from "./components/Pegawai/PegawaiList";
import AddPegawai from "./components/Pegawai/AddPegawai";
import EditPegawai from "./components/Pegawai/EditPegawai";
import GolonganList from "./components/Golongan/GolonganList";
import AddGolongan from "./components/Golongan/AddGolongan";
import EditGolongan from "./components/Golongan/EditGolongan";
import PangkatList from "./components/Pangkat/PangkatList";
import AddPangkat from "./components/Pangkat/AddPangkat";
import EditPangkat from "./components/Pangkat/EditPangkat";
import BidangList from "./components/Bidang/BidangList";
import AddBidang from "./components/Bidang/AddBidang";
import EditBidang from "./components/Bidang/EditBidang";
import UserList from "./components/Users/UserList";
import AddUser from "./components/Users/AddUser";
import EditUser from "./components/Users/EditUser";
import Dashboard from "./pages/Dashboard";
import Login from "./components/Login";
import Detail from "./components/home/Detail";
import EditPegawaiKontrak from "./components/Pegawai/EditPegawai_kontrak";
import AddPegawaiKontrak from "./components/Pegawai/AddPegawai_kontrak";
import DetailNonAsn from "./components/home/Detaill-nonasn";
import FileList from "./components/Files/FileList";
import AddFile from "./components/Files/AddFile";
import EditFile from "./components/Files/EditFile";
import FileHome from "./components/home/File";
import DetailUser from "./components/Users/DetailUser";
import PegawaiNonAsn from "./components/home/PegawaiNonAsn";
import PegawaiAsn from "./components/home/PegawaiAsn";
import Home from "./components/home/Home";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/pegawai-asn" element={<PegawaiAsn />} />
        <Route path="/pegawai-asn/views/:id" element={<Detail />} />

        <Route path="/pegawai-non-asn" element={<PegawaiNonAsn />} />
        <Route path="/pegawai-non-asn/views/:id" element={<DetailNonAsn />} />

        <Route path="/Dashboard" element={<Dashboard />} />
        <Route path="/login" element={<Login />} />
        <Route path="/berkas" element={<FileHome />} />

        <Route path="/pegawai" element={<PegawaiList />} />
        <Route path="/pegawai/adds" element={<AddPegawai />} />
        <Route path="/pegawai/add" element={<AddPegawaiKontrak />} />
        <Route path="/pegawai/edits/:id" element={<EditPegawai />} />
        <Route path="/pegawai/edit/:id" element={<EditPegawaiKontrak />} />

        <Route path="/golongan" element={<GolonganList />} />
        <Route path="/golongan/add" element={<AddGolongan />} />
        <Route path="/golongan/edit/:id" element={<EditGolongan />} />

        <Route path="/pangkat" element={<PangkatList />} />
        <Route path="/pangkat/add" element={<AddPangkat />} />
        <Route path="/pangkat/edit/:id" element={<EditPangkat />} />

        <Route path="/bidang" element={<BidangList />} />
        <Route path="/bidang/add" element={<AddBidang />} />
        <Route path="/bidang/edit/:id" element={<EditBidang />} />

        <Route path="/users" element={<UserList />} />
        <Route path="/users/add" element={<AddUser />} />
        <Route path="/users/edit/:id" element={<EditUser />} />
        <Route path="/users/view/:id" element={<DetailUser />} />

        <Route path="/Files" element={<FileList />} />
        <Route path="/Files/add" element={<AddFile />} />
        <Route path="/Files/edit/:id" element={<EditFile />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
