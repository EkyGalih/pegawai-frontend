import React from 'react'
import NavHome from '../components/layout/NavHome'
import Footer from '../components/layout/Footer'

const LayoutHome = ({children}) => {
    return (
        <React.Fragment>
            <NavHome />
            <main>{children}</main>
            <Footer />
        </React.Fragment>
    )
}

export default LayoutHome