import React from 'react'
import Layout from './Layout'
import UserList from '../components/Users/UserList'

const Users = () => {
    return (
        <Layout>
            <UserList />
        </Layout>
    )
}

export default Users