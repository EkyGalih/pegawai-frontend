import React, { useState, useEffect } from 'react';
import axios from "axios";
import ReactPaginate from 'react-paginate';
import { Link } from 'react-router-dom';
import Layout from '../../pages/Layout';
import { IoAdd, IoCheckboxSharp, IoClose, IoCreateOutline, IoEyeOutline, IoList, IoPeople, IoPersonAdd, IoPersonAddOutline, IoSearch, IoTrash } from 'react-icons/io5';
const PegawaiList = () => {
    const [pegawai, setPegawai] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(8);
    const [pages, setPages] = useState(0);
    const [rows, setRows] = useState(0);
    const [keyword, setKeyword] = useState("");
    const [query, setQuery] = useState("");
    const [msg, setMsg] = useState("");

    const [deleteId, setDeleteId] = useState("");
    const [isModal, setModal] = useState("");

    useEffect(() => {
        getPegawai();
        document.title = 'Pegawai Apps | Pegawai';
    }, [page, keyword]);

    const getPegawai = async () => {
        const response = await axios.get(
            `http://localhost:5000/pegawai?search_query=${keyword}&page=${page}&limit=${limit}`
        );
        setPegawai(response.data.result);
        setPage(response.data.page);
        setPages(response.data.totalPage);
        setRows(response.data.totalRows);
        console.log(response);
    };

    const changePage = ({ selected }) => {
        setPage(selected);
        if (selected === 9) {
            setMsg("If you don't find the data you are looking for, please search for data with specific keywords!");
        } else {
            setMsg("");
        }
    };

    const searchData = (e) => {
        e.preventDefault();
        setPage(0);
        setKeyword(query);
    };

    const handleClose = () => {
        setModal("");
    };

    const deletePegawai = async (pegawaiId) => {
        setDeleteId(pegawaiId);
        setModal("is-active");
    };

    const handleDeleteItem = async () => {
        try {
            await axios.delete(`http://localhost:5000/pegawai/${deleteId}`);
            getPegawai();
            setModal("");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPeople /> Pegawai</h1>
            <h2 className='subtitle'><IoList /> List of Pegawai</h2>
            <div className="container mt-5">
                <div className="dropdown is-hoverable mb-3">
                    <div className="dropdown-trigger">
                        <button className="button is-success" aria-haspopup="true" aria-controls="dropdown-menu4">
                            <span><IoAdd /> Add New Pegawai</span>
                            <span className="icon is-small">
                                <i className="fas fa-angle-down" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                    <div className="dropdown-menu" id="dropdown-menu4" role="menu">
                        <div className="dropdown-content">
                            <div className="dropdown-item">
                                <Link to="/pegawai/adds" className="button is-success mb-5"><IoPersonAddOutline /> PNS</Link>
                                <Link to="/pegawai/add" className="button is-success mb-5"><IoPersonAdd /> Non PNS</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <form onSubmit={searchData}>
                    <div className="field has-addons">
                        <div className="control is-expanded">
                            <input type="text" className="input" placeholder='Find something here...' value={query} onChange={(e) => setQuery(e.target.value)} />
                        </div>
                        <div className="control">
                            <button type='submit' className='button is-info'><IoSearch className='mr-1' /> Search</button>
                        </div>
                    </div>
                </form>
                <div className="columns is-multiline mt-2">

                    <div className={`modal ${isModal}`}>
                        <div className="modal-background"></div>
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Are you sure?</p>
                                <button className="delete" aria-label="close" onClick={handleClose}></button>
                            </header>
                            <footer className="modal-card-foot">
                                <button className="button is-danger" onClick={handleDeleteItem}><IoCheckboxSharp className='mr-1' /> yes</button>
                                <button className="button is-default" onClick={handleClose}><IoClose className='mr-1' /> No</button>
                            </footer>
                        </div>
                    </div>

                    {pegawai?.map((peg) => (
                        <div className="column is-one-quarter" key={peg.id}>
                            <div className="card">
                                <div className="card-image">
                                    <figure className="image is-6by4">
                                        <img src={peg.url} alt="Image" />
                                    </figure>
                                </div>

                                <div className="card-content">
                                    <div className="media">
                                        <div className="media-content">
                                            <p className="title is-5">{peg.name}</p>
                                            <p className="subtitle is-6">{peg.nip !== null ? peg.nip : peg.jabatan}</p>
                                        </div>
                                    </div>
                                </div>

                                <footer className='card-footer'>
                                    {peg.jenis_pegawai == 'pns' ?
                                        <Link to={`edits/${peg.uuid}`} className='card-footer-item'><IoCreateOutline /> Edit</Link>
                                        :
                                        <Link to={`edit/${peg.uuid}`} className='card-footer-item'><IoCreateOutline /> Edit</Link>
                                    }
                                    <Link to="" className='card-footer-item'><IoEyeOutline /> View</Link>
                                    <a onClick={() => deletePegawai(peg.uuid)} className='card-footer-item'><IoTrash /> Delete</a>
                                </footer>
                            </div>
                        </div>
                    ))}
                </div>
                <p>
                    Total Rows: {rows} Page: {rows ? page + 1 : 0} of {pages}
                </p>
                <p className='has-text-centered has-text-danger'>{msg}</p>
                <nav className="pagination is-centered" key={rows} role="navigation" aria-label='pagination'>
                    <ReactPaginate
                        previousLabel={"< Prev"}
                        nextLabel={"Next >"}
                        pageCount={Math.min(10, pages)}
                        onPageChange={changePage}
                        containerClassName={"pagination-list"}
                        pageLinkClassName={"pagination-link"}
                        previousLinkClassName={"pagination-previous"}
                        nextLinkClassName={"pagination-next"}
                        activeLinkClassName={"pagination-link is-current"}
                        disabledLinkClassName={"pagination-link is-disabled"}
                    />
                </nav>
            </div>
        </Layout>
    )
}

export default PegawaiList