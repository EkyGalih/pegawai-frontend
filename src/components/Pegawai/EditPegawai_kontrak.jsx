import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Layout from '../../pages/Layout';
import { IoCreateOutline, IoPeople, IoPlayBack, IoSave } from 'react-icons/io5';

const EditPegawaiKontrak = () => {
    const [name, setName] = useState("");
    const [tempat_lahir, setTLahir] = useState("");
    const [tanggal_lahir, setTglLahir] = useState("");
    const [jabatan, setJabatan] = useState("");
    const [masa_kerja_golongan, setMkg] = useState("");
    const [pendidikan, setPendidikan] = useState("");
    const [umur, setUmur] = useState("");
    const [jenis_kelamin, setJk] = useState("");
    const [agama, setAgama] = useState("");
    const [no_sk, setSk] = useState("");
    const [no_rekening, setNoRekening] = useState("");
    const [nama_rekening, setNamaRekening] = useState("");
    const [bidangUuid, setBidang] = useState("");

    const [bidangs, setBidangs] = useState([]);

    const [file, setFile] = useState("");
    const [preview, setPreview] = useState("");
    const { id } = useParams();
    const navigate = useNavigate();

    const loadImage = (e) => {
        const image = e.target.files[0];
        setFile(image);
        setPreview(URL.createObjectURL(image));
    };

    useEffect(() => {
        getBidang();
        getPegawaiById();
        document.title = 'Pegawai Apps | Edit Pegawai PNS';
    }, []);

    const getPegawaiById = async () => {
        const response = await axios.get(`http://localhost:5000/pegawai/${id}`);
        setName(response.data.name);
        setTLahir(response.data.tempat_lahir);
        setTglLahir(response.data.tanggal_lahir);
        setBidang(response.data.bidangUuid);
        setJabatan(response.data.jabatan);
        setMkg(response.data.masa_kerja_golongan);
        setPendidikan(response.data.pendidikan);
        setUmur(response.data.umur);
        setJk(response.data.jenis_kelamin);
        setAgama(response.data.agama);
        setSk(response.data.no_sk);
        setNamaRekening(response.data.nama_rekening);
        setNoRekening(response.data.no_rekening);
        setFile(response.data.foto);
        setPreview(response.data.url);
    }

    const getBidang = async () => {
        const responseB = await axios.get("http://localhost:5000/bidangs");
        setBidangs(responseB.data);
    };

    const updatePegawai = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("name", name);
        formData.append("tempat_lahir", tempat_lahir);
        formData.append("tanggal_lahir", tanggal_lahir);
        formData.append("jabatan", jabatan);
        formData.append("masa_kerja_golongan", masa_kerja_golongan);
        formData.append("pendidikan", pendidikan);
        formData.append("umur", umur);
        formData.append("jenis_kelamin", jenis_kelamin);
        formData.append("agama", agama);
        formData.append("bidangUuid", bidangUuid);
        formData.append("file", file);
        try {
            await axios.patch(`http://localhost:5000/pegawai/${id}`, formData, {
                headers: {
                    "Content-type": "multipart/form-data"
                }
            });
            navigate("/pegawai");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPeople /> Pegawai</h1>
            <h2 className='subtitle'><IoCreateOutline /> Edit Pegawai</h2>
            <div className="columns is-centered">
                <div className="column is-half">
                    <form onSubmit={updatePegawai}>
                        <div className="field">
                            <label className="label">Nama Pegawai</label>
                            <div className="control">
                                <input type="text" className="input" value={name} onChange={(e) => setName(e.target.value)} placeholder="Nama Pegawai" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Tempat/Tanggal Lahir</label>
                            <div className="control">
                                <div className="columns">
                                    <div className="column is-three-quarters">
                                        <input type="text" className="input" value={tempat_lahir} onChange={(e) => setTLahir(e.target.value)} placeholder='Tempat Lahir' />
                                    </div>
                                    <div className="column is-one-quarter">
                                        <input type="date" className="input" value={tanggal_lahir} onChange={(e) => setTglLahir(e.target.value)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Bidang</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={bidangUuid} onChange={(e) => setBidang(e.target.value)}>
                                        <option value="">------</option>
                                        {bidangs.map((bidang) => (
                                            <option value={bidang.uuid}>{bidang.nama_bidang}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Jabatan</label>
                            <div className="control">
                                <input type="text" className="input" value={jabatan} onChange={(e) => setJabatan(e.target.value)} placeholder="Jabatan" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Masa Kerja</label>
                            <div className="control">
                                <input type="text" className="input" value={masa_kerja_golongan} onChange={(e) => setMkg(e.target.value)} placeholder="Masa Kerja" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Pendidikan</label>
                            <div className="control">
                                <input type="text" className="input" value={pendidikan} onChange={(e) => setPendidikan(e.target.value)} placeholder="Pendidikan" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Umur</label>
                            <div className="control">
                                <input type="number" className="input" value={umur} onChange={(e) => setUmur(e.target.value)} placeholder="Umur" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Jenis Kelamin</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={jenis_kelamin} onChange={(e) => setJk(e.target.value)}>
                                        <option value="-">--------</option>
                                        <option value="pria">Pria</option>
                                        <option value="wanita">Wanita</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Agama</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={agama} onChange={(e) => setAgama(e.target.value)}>
                                        <option value="-">--------</option>
                                        <option value="islam">Islam</option>
                                        <option value="hindu">hindu</option>
                                        <option value="kristen">kristen</option>
                                        <option value="budha">budha</option>
                                        <option value="konghucu">konghucu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Nomor SK</label>
                            <div className="control">
                                <input type="text" className="input" value={no_sk} onChange={(e) => setSk(e.target.value)} placeholder="Kenaikan Pangkat" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Nomor Rekening</label>
                            <div className="control">
                                <div className="columns">
                                    <div className="column is-half">
                                        <input type="text" className='input' value={nama_rekening} onChange={(e) => setNamaRekening(e.target.value)} placeholder='Nama Bank' />
                                    </div>
                                    <div className="column is-half">
                                        <input type="text" className="input" value={no_rekening} onChange={(e) => setNoRekening(e.target.value)} placeholder="Nomor Rekening" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Foto</label>
                            <div className="control">
                                <div className="columns">
                                    <div className="column is-three-quarters">
                                        <div className="file">
                                            <label className="file-label">
                                                <input type="file" className="file-input" onChange={loadImage} />
                                                <span className='file-cta'>
                                                    <span className='file-label'>Choose a file...</span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="column is-one-fifth">
                                        {preview ? (
                                            <figure className="image is-6by4">
                                                <img src={preview} alt="Preview Image" />
                                            </figure>
                                        ) : (
                                            ""
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="field">
                            <div className="control">
                                <button type="submit" className="button is-success mr-2"><IoSave /> Update</button>
                                <Link to="/pegawai" className='button is-default'><IoPlayBack className='mr-1' />Back</Link>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default EditPegawaiKontrak