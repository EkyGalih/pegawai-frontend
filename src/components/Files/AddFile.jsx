import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Layout from '../../pages/Layout';
import { IoAdd, IoPeople, IoPlayBack } from 'react-icons/io5';

const AddFile = () => {
    const { user } = useSelector((state) => state.auth);

    const [namaFile, setNamaFile] = useState("");
    const [jenisFile, setJenisFile] = useState("");
    const [extFile, setExtFile] = useState("");
    const [bidangUuid, setBidang] = useState("");
    const [pegawaiUuid, setPegawai] = useState(user && user.pegawaiUuid);
    const [msg, setMsg]= useState("");
    const [error, setError] = useState("");

    const [file, setFile] = useState("");
    const [preview, setPreview] = useState("");
    const navigate = useNavigate();

    const loadFile = (e) => {
        const image = e.target.files[0];
        const ext = image.name.split('.');
        setNamaFile(image.name);
        setFile(image);
        setExtFile(ext[1]);
        setPreview(URL.createObjectURL(image));
    };

    useEffect(() => {
        getPegawaiById();
        document.title = 'Pegawai Apps | Add File';
    }, []);

    const getPegawaiById = async () => {
        const response = await axios.get(`http://localhost:5000/pegawai/${pegawaiUuid}`);
        setBidang(response.data.bidangUuid);
    }

    const saveFile = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("nama_files", namaFile);
        formData.append("files", file);
        formData.append("jenis_file", jenisFile);
        formData.append("ext_file", extFile);
        formData.append("bidangUuid", bidangUuid);
        formData.append("pegawaiUuid", pegawaiUuid);
        formData.append("file", file);
        try {
            await axios.post("http://localhost:5000/file", formData, {
                headers: {
                    "Content-type": "multipart/form-data"
                }
            });
            navigate("/files");
        } catch (error) {
            if (error.response) {
                setMsg(error.response.data.msg);
                setError('notification is-danger');
            }
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPeople /> Files</h1>
            <h2 className='subtitle'><IoAdd /> Add New File</h2>
            <div className="columns is-centered">
                <div className="column is-half">
                    <p className={error}>{msg}</p>
                    <form onSubmit={saveFile}>
                        <div className="field">
                            <label className="label">Jenis File</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={jenisFile} onChange={(e) => setJenisFile(e.target.value)}>
                                        <option value="-">--------------</option>
                                        <option value="LKJIP">LKJIP</option>
                                        <option value="PMPRB">PMPRB</option>
                                        <option value="SAKIP">SAKIP</option>
                                        <option value="SPIP">SPIP</option>
                                        <option value="PROSES BISNIS">PROSES BISNIS</option>
                                        <option value="RKPD">RKPD</option>
                                        <option value="RENSTRA">RENSTRA</option>
                                        <option value="RENJA">RENJA</option>
                                        <option value="APBD">APBD</option>
                                        <option value="APBDP">APBDP</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Files</label>
                            <div className="control">
                                <div className="columns">
                                    <div className="column is-three-quarters">
                                        <div className="file">
                                            <label className="file-label">
                                                <input type="file" className="file-input" onChange={loadFile} />
                                                <span className='file-cta'>
                                                    <span className='file-label'>Choose a file...</span>
                                                </span>
                                            </label>
                                        </div>
                                        <input type="hidden" value={namaFile} onChange={(e) => setNamaFile(e.target.value)} />
                                        <input type="hidden" value={extFile} onChange={(e) => setExtFile(e.target.value)} />
                                        <input type="hidden" value={pegawaiUuid} onChange={(e) => setPegawai(e.target.value)} />
                                        <input type="hidden" value={bidangUuid} onChange={(e) => setBidang(e.target.value)} />
                                    </div>
                                    <div className="column is-one-fifth">
                                        {preview ? (
                                            <figure className="image is-6by4">
                                                <img src={preview} alt="Preview Image" />
                                            </figure>
                                        ) : (
                                            ""
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="field">
                            <div className="control">
                                <button type="submit" className="button is-success mr-2"><IoAdd /> Add</button>
                                <Link to="/files" className='button is-default'><IoPlayBack className='mr-1' />Back</Link>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default AddFile