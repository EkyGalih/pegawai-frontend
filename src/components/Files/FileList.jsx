import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import Layout from '../../pages/Layout'
import { IoAdd, IoCheckboxSharp, IoClose, IoCreateOutline, IoFileTrayFullSharp, IoList, IoReloadOutline, IoSearch, IoTrash } from 'react-icons/io5'
import { Link } from 'react-router-dom'

const FileList = () => {
    const [files, setFiles] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(10);
    const [pages, setPages] = useState(0);
    const [rows, setRows] = useState(0);
    const [keyword, setKeyword] = useState("");
    const [query, setQuery] = useState("");
    const [msg, setMsg] = useState("");

    const [deleteId, setDeleteId] = useState("");
    const [isModal, setModal] = useState("");

    useEffect(() => {
        getFiles();
        document.title = 'Pegawai Apps | Files';
    }, [page, keyword]);

    const getFiles = async () => {
        const response = await axios.get(
            `http://localhost:5000/file?search_query=${keyword}&page=${page}&limit=${limit}`
        );
        setFiles(response.data.result);
        setPage(response.data.page);
        setPages(response.data.totalPage);
        setRows(response.data.totalRows);
    }

    const changePage = ({ selected }) => {
        setPage(selected);
        if (selected === 9) {
            setMsg("If you don't find the data you are looking for, please search for data with specific keywords!");
        } else {
            setMsg("");
        }
    };

    const searchData = (e) => {
        e.preventDefault();
        setPage(0);
        setKeyword(query);
    };

    const handleClose = () => {
        setModal("");
    };

    const deleteFile = async (fileId) => {
        setDeleteId(fileId);
        setModal("is-active");
    };

    const handleDeleteItem = async () => {
        try {
            await axios.delete(`http://localhost:5000/file/${deleteId}`);
            getFiles();
            setModal("");
        } catch (error) {
            console.log(error);
        }
    };

    const resetKeyword = () => {
        setKeyword("");
    };

    return (
        <Layout>
            <h1 className='title'><IoFileTrayFullSharp /> Files</h1>
            <h2 className='subtitle'><IoList /> List of Files</h2>
            <div className="container mt-5">
                <div className="columns is-half">

                    <div className={`modal ${isModal}`}>
                        <div className="modal-background"></div>
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Are you sure?</p>
                                <button className="delete" aria-label="close" onClick={handleClose}></button>
                            </header>
                            <footer className="modal-card-foot">
                                <button className="button is-danger" onClick={handleDeleteItem}><IoCheckboxSharp className='mr-1' /> yes</button>
                                <button className="button is-default" onClick={handleClose}><IoClose className='mr-1' /> No</button>
                            </footer>
                        </div>
                    </div>


                    <div className="column">
                        <Link to="add" className='button is-success mb-2'><IoAdd /> Add New File</Link>

                        <form onSubmit={searchData}>
                            <div className="field has-addons">
                                <div className="control is-expanded">
                                    <input type="text" className="input" placeholder='Find something here...' value={query} onChange={(e) => setQuery(e.target.value)} />
                                </div>
                                <div className="control">
                                    <button type='submit' className='button is-info'><IoSearch className='mr-1' /> Search</button>
                                    {
                                        keyword === '' ? '' : <button onClick={() => resetKeyword()} className='button is-default'><IoReloadOutline className='mr-1' /> Reset</button>
                                    }
                                </div>
                            </div>
                        </form>

                        <table className='table is-striped is-hoverable is-fullwidth mt-2'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama File</th>
                                    <th>Jenis File</th>
                                    <th>Ektension File</th>
                                    <th>Uploaded By</th>
                                    <th>From</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {files.map((file, index) => (
                                    <tr key={file.uuid}>
                                        <td>{index + 1}</td>
                                        <td>{file.nama_files}</td>
                                        <td>{file.jenis_file}</td>
                                        <td>{file.ext_file}</td>
                                        <td>{file.pegawai.name}</td>
                                        <td>{file.bidang.nama_bidang}</td>
                                        <td>
                                            <Link to={`/files/edit/${file.uuid}`} className='button is-warning mr-2'><IoCreateOutline /> Edit</Link>
                                            <button onClick={() => deleteFile(file.uuid)} className='button is-danger'><IoTrash /> Delete</button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <p>
                            Total Rows: {rows} Page: {rows ? page + 1 : 0} of {pages}
                        </p>
                        <p className='has-text-centered has-text-danger'>{msg}</p>
                        <nav className="pagination is-centered" key={rows} role="navigation" aria-label='pagination'>
                            <ReactPaginate
                                previousLabel={"< Prev"}
                                nextLabel={"Next >"}
                                pageCount={Math.min(10, pages)}
                                onPageChange={changePage}
                                containerClassName={"pagination-list"}
                                pageLinkClassName={"pagination-link"}
                                previousLinkClassName={"pagination-previous"}
                                nextLinkClassName={"pagination-next"}
                                activeLinkClassName={"pagination-link is-current"}
                                disabledLinkClassName={"pagination-link is-disabled"}
                            />
                        </nav>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default FileList