import React, { useState } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { IoPerson, IoSpeedometer, IoHome, IoInfinite, IoPricetag, IoLogOut, IoPeople, IoFileTrayFullSharp } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { LogOut, reset } from "../../features/authSlice";

const Sidebar = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { user } = useSelector((state) => state.auth);
    const [uri] = useState(window.location.pathname);

    const logout = () => {
        dispatch(LogOut());
        dispatch(reset());
        navigate("/login");
    };

    return (
        <div>
            <aside className="menu pl-2 has-shadow">
                <p className="menu-label">
                    Menu
                </p>
                <ul className="menu-list">
                    <li><NavLink to={"/dashboard"} className={uri === '/dashboard' ? 'is-active' : ''}><IoSpeedometer /> Dashboard</NavLink></li>
                    <li><NavLink to={"/users"} className={uri === '/users' ? 'is-active' : ''}><IoPerson /> Users</NavLink></li>
                    <li><NavLink to={"/Files"} className={uri === '/files' ? 'is-active' : ''}><IoFileTrayFullSharp /> Files</NavLink></li>
                    <li><NavLink to={"/pegawai"} className={uri === '/pegawai' ? 'is-active' : ''}><IoPeople /> Pegawai</NavLink></li>
                </ul>
                <p className="menu-label">
                    AddOn
                </p>
                <ul className="menu-list">
                    <li><NavLink to={"/bidang"} className={uri === '/bidang' ? 'is-active' : ''}><IoHome /> Bidang</NavLink></li>
                    <li><NavLink to={"/pangkat"} className={uri === '/pangkat' ? 'is-active' : ''}><IoPricetag /> Pangkat</NavLink></li>
                    <li><NavLink to={"/golongan"} className={uri === '/pangkat' ? 'is-active' : ''}><IoInfinite /> Golongan</NavLink></li>
                </ul>
                <p className="menu-label">
                    Settings
                </p>
                <ul className="menu-list">
                    <li>
                        <button onClick={logout} className='button is-white'><IoLogOut /> Logout</button>
                    </li>
                </ul>
            </aside>
        </div>
    )
}

export default Sidebar