import React, { useEffect, useState } from 'react'
import { IoFileTrayFullSharp, IoLogInOutline, IoLogOutOutline, IoPeople } from 'react-icons/io5'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom'
import { LogOut, getMe, reset } from '../../features/authSlice';

const NavHome = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { isError } = useSelector((state) => state.auth);

    const [uri] = useState(window.location.pathname);

    useEffect(() => {
        dispatch(getMe());
    }, [dispatch]);

    const logout = () => {
        dispatch(LogOut());
        dispatch(reset());
        navigate("/");
    };
    return (
        <section className="hero is-info is-small">
            {/* <!-- Hero head: will stick at the top --> */}
            <div className="hero-head">
                <nav className="navbar">
                    <div className="container">
                        <div className="navbar-brand">
                            <Link to="/" className="navbar-item">
                                <img src="https://bulma.io/images/bulma-type-white.png" alt="Logo" />
                            </Link>
                        </div>
                        <div id="navbarBasicExample" className="navbar-menu">
                            <div className="navbar-start">
                                <Link to="/pegawai-asn" className={uri === '/pegawai-asn' ? 'navbar-item is-active' : 'navbar-item'}>
                                    <IoPeople className='mr-1' /> Pegawai
                                </Link>

                                <Link to="/berkas" className={uri === '/berkas' ? 'navbar-item is-active' : 'navbar-item'}>
                                    <IoFileTrayFullSharp className='mr-1' /> Files
                                </Link>
                            </div>
                        </div>
                        <div className="navbar-end">
                            <div className="navbar-item">
                                <div className="buttons">
                                    {
                                        isError === true ?
                                            <Link to="/login" className="button is-outlined is-light">
                                                <IoLogInOutline /> Log in
                                            </Link>
                                            :
                                            <button className='button is-outlined is-light' onClick={() => logout()}><IoLogOutOutline /> Log Out</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </section>
    )
}

export default NavHome