import React from 'react'

const Footer = () => {
    return (
        <div>
            <footer className="footer">
                <div className="content has-text-centered">
                    <p>
                        <strong>Apps Pegawai</strong> by <a target="_blank" href="https://instagram.com/ekygalih_">ITeam BPKAD Prov.NTB</a>. The source code is opensource.
                    </p>
                </div>
            </footer>
        </div>
    )
}

export default Footer