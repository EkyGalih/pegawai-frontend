import React, { useState, useEffect } from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate';
import { Link } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoAdd, IoCheckboxSharp, IoClose, IoCreateOutline, IoList, IoPricetag, IoSearch, IoTrash } from 'react-icons/io5';

const PangkatList = () => {
    const [pangkat, setPangkat] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(5);
    const [pages, setPages] = useState(0);
    const [rows, setRows] = useState(0);
    const [keyword, setKeyword] = useState("");
    const [query, setQuery] = useState("");
    const [msg, setMsg] = useState("");

    const [deleteId, setDeleteId] = useState("");
    const [isModal, setModal] = useState("");

    const handleClose = () => {
        setModal("");
    };

    useEffect(() => {
        getPangkat();
        document.title = 'Pegawai Apps | Pangkat';
    }, [page, keyword]);

    const getPangkat = async () => {
        const response = await axios.get(
            `http://localhost:5000/pangkat?search_query=${keyword}&page=${page}&limit=${limit}`
        );
        setPangkat(response.data.result);
        setPage(response.data.page);
        setPages(response.data.totalPage);
        setRows(response.data.totalRows);
    };

    const changePage = ({ selected }) => {
        setPage(selected);
        if (selected === 9) {
            setMsg("If you don't find the data you are looking for, please search for data with specific keywords!");
        } else {
            setMsg("");
        }
    };

    const searchData = (e) => {
        e.preventDefault();
        setPage(0);
        setKeyword(query);
    };

    const deletePangkat = async (pangkatId) => {
        setDeleteId(pangkatId);
        setModal("is-active");
    };

    const handleDeleteItem = async () => {
        try {
            await axios.delete(`http://localhost:5000/pangkat/${deleteId}`);
            getPangkat();
            setModal("");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPricetag /> Pangkat</h1>
            <h2 className='subtitle'><IoList /> List of Pangkat</h2>
            <div className="container mt-5">
                <div className="columns is-half">

                    <div className={`modal ${isModal}`}>
                        <div className="modal-background"></div>
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Are you sure?</p>
                                <button className="delete" aria-label="close" onClick={handleClose}></button>
                            </header>
                            <footer className="modal-card-foot">
                                <button className="button is-danger" onClick={handleDeleteItem}><IoCheckboxSharp className='mr-1' /> yes</button>
                                <button className="button is-default" onClick={handleClose}><IoClose className='mr-1' /> No</button>
                            </footer>
                        </div>
                    </div>

                    <div className="column">
                        <Link to="/pangkat/add" className='button is-success mb-5'><IoAdd /> Add New Pangkat</Link>
                        <form onSubmit={searchData}>
                            <div className="field has-addons">
                                <div className="control is-expanded">
                                    <input type="text" className="input" placeholder='Find something here...' value={query} onChange={(e) => setQuery(e.target.value)} />
                                </div>
                                <div className="control">
                                    <button type='submit' className='button is-info'><IoSearch className='mr-1' /> Search</button>
                                </div>
                            </div>
                        </form>
                        <table className='table is-striped is-hoverable is-fullwidth mt-2'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Pangkat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {pangkat.map((pang, index) => (
                                    <tr key={pang.uuid}>
                                        <td>{index + 1}</td>
                                        <td>{pang.nama_pangkat}</td>
                                        <td>
                                            <Link to={`/pangkat/edit/${pang.uuid}`} className='button is-warning mr-2'><IoCreateOutline /> Edit</Link>
                                            <button onClick={() => deletePangkat(pang.uuid)} className='button is-danger'><IoTrash /> Delete</button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <p>
                            Total Rows: {rows} Page: {rows ? page + 1 : 0} of {pages}
                        </p>
                        <p className='has-text-centered has-text-danger'>{msg}</p>
                        <nav className="pagination is-centered" key={rows} role="navigation" aria-label='pagination'>
                            <ReactPaginate
                                previousLabel={"< Prev"}
                                nextLabel={"Next >"}
                                pageCount={Math.min(10, pages)}
                                onPageChange={changePage}
                                containerClassName={"pagination-list"}
                                pageLinkClassName={"pagination-link"}
                                previousLinkClassName={"pagination-previous"}
                                nextLinkClassName={"pagination-next"}
                                activeLinkClassName={"pagination-link is-current"}
                                disabledLinkClassName={"pagination-link is-disabled"}
                            />
                        </nav>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default PangkatList