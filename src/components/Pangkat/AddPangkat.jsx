import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoAdd, IoPlayBack, IoPricetag } from 'react-icons/io5';

const AddPangkat = () => {
    const [nama_pangkat, setPangkat] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        document.title = 'Pegawai Apps | Add Pangkat';
    });

    const savePangkat = async (e) => {
        e.preventDefault();
        try {
            await axios.post("http://localhost:5000/pangkat", {
                nama_pangkat: nama_pangkat
            });
            navigate("/pangkat");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPricetag/> Pangkat</h1>
            <h2 className='subtitle'><IoAdd/> Add Pangkat</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <form onSubmit={savePangkat}>
                        <div className="field">
                            <label className="label">Pangkat</label>
                            <div className="control">
                                <input type="text" className="input" value={nama_pangkat} onChange={(e) => setPangkat(e.target.value)} placeholder="Nama pangkat" autoFocus />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoAdd/> Add</button>
                                <Link to="/pangkat" className='button is-default'><IoPlayBack className='mr-1'/> Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default AddPangkat