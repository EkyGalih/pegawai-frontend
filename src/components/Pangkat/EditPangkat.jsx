import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate, useParams } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoCreateOutline, IoPlayBack, IoPricetag, IoSave } from 'react-icons/io5';

const EditPangkat = () => {
    const [nama_pangkat, setPangkat] = useState("");
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getPangkatById();
        document.title = 'Pegawai Apps | Edit Pangkat';
    }, []);

    const getPangkatById = async () => {
        const response = await axios.get(`http://localhost:5000/pangkat/${id}`);
        setPangkat(response.data.nama_pangkat);
    }

    const updatePangkat = async (e) => {
        e.preventDefault();
        try {
            await axios.patch(`http://localhost:5000/pangkat/${id}`, {
                nama_pangkat: nama_pangkat
            });
            navigate("/pangkat");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPricetag/> Pangkat</h1>
            <h2 className='subtitle'><IoCreateOutline/> Edit Pangkat</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <form onSubmit={updatePangkat}>
                        <div className="field">
                            <label className="label">Pangkat</label>
                            <div className="control">
                                <input type="text" className="input" value={nama_pangkat} onChange={(e) => setPangkat(e.target.value)} placeholder="Nama Pangkat" />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoSave/> Update</button>
                                <Link to="/pangkat" className='button is-default'><IoPlayBack className='mr-1'/> Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default EditPangkat