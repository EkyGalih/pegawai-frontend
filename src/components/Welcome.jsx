import React from 'react'
import { useSelector } from 'react-redux'

const Welcome = () => {
    const { user } = useSelector((state) => state.auth);

    return (
        <div>
            <section className="hero is-link is-half-with-navbar">
                <div className="hero-body">
                    <p className="title">
                        Welcome Back <strong>{user && user.username}</strong>
                    </p>
                </div>
            </section>
        </div>
    )
}

export default Welcome