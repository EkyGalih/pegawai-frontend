import React from 'react'
import LayoutHome from '../../pages/LayoutHome'
import { Link } from 'react-router-dom'
import { IoGlobeOutline } from 'react-icons/io5'

const Home = () => {
    return (
        <LayoutHome>
            <div className="container mt-5">
                <h2 className='title'>SELAMAT DATANG DI WEBSITE INFORMASI LOKAL BPKAD</h2>
                <div className="columns is-multiline mt-1">
                    <div className="column is-one-quarter">
                        <div className="card">
                            <div className="card-image">
                                <figure className="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image" />
                                </figure>
                            </div>
                            <div className="card-content">
                                <div className="media">
                                    <div className="media-content">
                                        <p className="title is-4">PEGAWAI</p>
                                    </div>
                                </div>

                                <div className="content">
                                    Lorem ipsum dolor sit amet.
                                </div>
                                <footer className='card-footer'>
                                    <Link to="/pegawai-asn" className='card-footer-item'><IoGlobeOutline className='mr-1' /> Visit</Link>
                                </footer>
                            </div>
                        </div>
                    </div>
                    <div className="column is-one-quarter">
                        <div className="card">
                            <div className="card-image">
                                <figure className="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image" />
                                </figure>
                            </div>
                            <div className="card-content">
                                <div className="media">
                                    <div className="media-content">
                                        <p className="title is-4">BERKAS</p>
                                    </div>
                                </div>

                                <div className="content">
                                    Lorem ipsum dolor sit amet.
                                </div>
                                <footer className='card-footer'>
                                    <Link to="/berkas" className='card-footer-item'><IoGlobeOutline className='mr-1' /> Visit</Link>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutHome>
    )
}

export default Home