import axios from 'axios';
import React, { useState, useEffect } from 'react'
import { IoPlayBack } from 'react-icons/io5';
import { Link, useParams } from 'react-router-dom';
import LayoutHome from '../../pages/LayoutHome';

const DetailNonAsn = () => {
    const [name, setName] = useState("");
    const [tempat_lahir, setTLahir] = useState("");
    const [tanggal_lahir, setTglLahir] = useState("");
    const [jabatan, setJabatan] = useState("");
    const [masa_kerja_golongan, setMkg] = useState("");
    const [pendidikan, setPendidikan] = useState("");
    const [NoSk, setNoSk] = useState("");
    const [NoRekening, setNoRekening] = useState("");
    const [NamaRekening, setNamaRekening] = useState("");
    const [jenisPegawai, setJenisPegawai] = useState("");
    const [umur, setUmur] = useState("");
    const [jenis_kelamin, setJk] = useState("");
    const [agama, setAgama] = useState("");
    const [bidang, setBidang] = useState("");
    const [file, setFile] = useState("");
    const [preview, setPreview] = useState("");
    const { id } = useParams();

    useEffect(() => {
        getPegawaiById();
    }, []);

    const getPegawaiById = async () => {
        const response = await axios.get(`http://localhost:5000/pegawai/${id}`);
        setName(response.data.name);
        setJenisPegawai(response.data.jenis_pegawai);
        setNoSk(response.data.no_sk);
        setNoRekening(response.data.no_rekening);
        setNamaRekening(response.data.nama_rekening);
        setTLahir(response.data.tempat_lahir);
        setTglLahir(response.data.tanggal_lahir);
        setMkg(response.data.masa_kerja_golongan);
        setJabatan(response.data.jabatan);
        setPendidikan(response.data.pendidikan);
        setBidang(response.data.bidang.nama_bidang);
        setUmur(response.data.umur);
        setJk(response.data.jenis_kelamin);
        setAgama(response.data.agama);
        setFile(response.data.foto);
        setPreview(response.data.url);
    }

    return (
        <LayoutHome>
            <div>
                <section className="hero is-primary is-small">
                    {/* <!-- Hero head: will stick at the top --> */}
                    <div className="hero-head">
                        <nav className="navbar">
                            <div className="container">
                                <div className="navbar-brand">
                                    <Link to="/" className="navbar-item">
                                        <img src="https://bulma.io/images/bulma-type-white.png" alt="Logo" />
                                    </Link>
                                </div>
                            </div>
                        </nav>
                    </div>
                </section>

                <div className="container mt-5">
                    <div className="columns is-centered">
                        <div className="column is-two-fifths">
                            <div className="card">
                                <div className="card-image">
                                    <figure className="image is-2by3">
                                        <img src={preview} alt="Placeholder image" />
                                    </figure>
                                </div>
                                <div className="card-content">
                                    <div className="media">
                                        <div className="media-content">
                                            <p className="title is-4">{name}</p>
                                            <p className="subtitle is-6">{jabatan}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <table className='table is-fullwidth'>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div className="tags are-medium">
                                                <strong className='tag is-info is-light'>
                                                    {jenisPegawai !== null ? 'Non ASN' : ''}
                                                </strong>
                                            </div>
                                        </td>
                                        <td><strong>Kelahiran </strong>{tempat_lahir + ', ' + tanggal_lahir}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Bidang </strong>{bidang}</td>
                                        <td>{'Masa Kerja ' + masa_kerja_golongan}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Pendidikan </strong>{pendidikan}</td>
                                        <td><strong>Diklat </strong></td>
                                    </tr>
                                    <tr>
                                        <td>{jenis_kelamin.toUpperCase()}</td>
                                        <td><strong>Usia </strong>{umur + ' Tahun'}</td>
                                    </tr>
                                    <tr>
                                        <td>{agama.toUpperCase()}</td>
                                        <td>No.SK <strong>{NoSk}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Rekening <strong>{NoRekening}</strong></td>
                                        <td><strong>{NamaRekening}</strong></td>
                                    </tr>
                                </tbody>
                                <div className="field mt-5">
                                    <Link to="/pegawai-non-asn" className='button is-block is-primary'><IoPlayBack className='mr-1' /> Kembali</Link>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutHome>
    )
}

export default DetailNonAsn