import axios from 'axios';
import React, { useState, useEffect } from 'react'
import { IoPlayBack } from 'react-icons/io5';
import { Link, useParams } from 'react-router-dom';
import LayoutHome from '../../pages/LayoutHome';

const Detail = () => {
    const [nip, setNip] = useState("");
    const [name, setName] = useState("");
    const [tempat_lahir, setTLahir] = useState("");
    const [tanggal_lahir, setTglLahir] = useState("");
    const [jabatan, setJabatan] = useState("");
    const [masa_kerja_golongan, setMkg] = useState("");
    const [diklat, setDiklat] = useState("");
    const [pendidikan, setPendidikan] = useState("");
    const [umur, setUmur] = useState("");
    const [jenis_kelamin, setJk] = useState("");
    const [agama, setAgama] = useState("");
    const [kenaikan_pangkat, setKp] = useState("");
    const [jenisPegawai, setJenisPegawai] = useState("");
    const [batas_pensiun, setPensiun] = useState("");
    const [pangkat, setPangkat] = useState("");
    const [golongan, setGolongan] = useState("");
    const [bidang, setBidang] = useState("");
    const [file, setFile] = useState("");
    const [preview, setPreview] = useState("");
    const { id } = useParams();

    useEffect(() => {
        getPegawaiById();
    }, []);

    const getPegawaiById = async () => {
        const response = await axios.get(`http://localhost:5000/pegawai/${id}`);
        setNip(response.data.nip);
        setJenisPegawai(response.data.jenis_pegawai);
        setName(response.data.name);
        setTLahir(response.data.tempat_lahir);
        setTglLahir(response.data.tanggal_lahir);
        setGolongan(response.data.golongan.nama_golongan);
        setPangkat(response.data.pangkat.nama_pangkat);
        setBidang(response.data.bidang.nama_bidang);
        setMkg(response.data.masa_kerja_golongan);
        setJabatan(response.data.jabatan);
        setDiklat(response.data.diklat);
        setPendidikan(response.data.pendidikan);
        setUmur(response.data.umur);
        setJk(response.data.jenis_kelamin);
        setAgama(response.data.agama);
        setKp(response.data.kenaikan_pangkat);
        setPensiun(response.data.batas_pensiun);
        setFile(response.data.foto);
        setPreview(response.data.url);
    }

    return (
        <LayoutHome>
            <div>
                <div className="container mt-5">
                    <div className="columns is-centered">
                        <div className="column is-two-fifths">
                            <div className="card">
                                <div className="card-image">
                                    <figure className="image is-2by3">
                                        <img src={preview} alt="Placeholder image" />
                                    </figure>
                                </div>
                                <div className="card-content">
                                    <div className="media">
                                        <div className="media-content">
                                            <p className="title is-4">{name}</p>
                                            <p className="subtitle is-6">{nip}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <table className='table is-fullwidth'>
                                <tbody>
                                    <tr>
                                        <td><strong>Jabatan </strong>{jabatan}</td>
                                        <td><strong>Kelahiran </strong>{tempat_lahir + ', ' + tanggal_lahir}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Golongan </strong>{pangkat}</td>
                                        <td><strong>Pangkat </strong>{golongan}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Bidang </strong>{bidang}</td>
                                        <td>{'Masa Kerja ' + masa_kerja_golongan}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Pendidikan </strong>{pendidikan}</td>
                                        <td><strong>Diklat </strong>{diklat}</td>
                                    </tr>
                                    <tr>
                                        <td>{jenis_kelamin.toUpperCase()}</td>
                                        <td><strong>Usia </strong>{umur + ' Tahun'}</td>
                                    </tr>
                                    <tr>
                                        <td>Kenaikan Pangkat <strong>{kenaikan_pangkat}</strong></td>
                                        <td><strong>Pensiun </strong>{batas_pensiun}</td>
                                    </tr>
                                    <tr>
                                        <td>{agama.toUpperCase()}</td>
                                        <td>
                                            <div className="tags are-medium">
                                                <strong className="tag is-primary is-light">{jenisPegawai !== null ? 'ASN' : ''}</strong>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <div className="field mt-5">
                                    <Link to="/pegawai-asn" className='button is-block is-primary'><IoPlayBack className='mr-1' /> Kembali</Link>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutHome>
    )
}

export default Detail