import React, { useState, useEffect } from 'react'
import { IoEyeOutline, IoFileTrayFullSharp, IoPeople, IoReloadOutline, IoSearch } from 'react-icons/io5'
import { Link } from 'react-router-dom'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import LayoutHome from '../../pages/LayoutHome'
import { useSelector } from 'react-redux'

const PegawaiAsn = () => {
    const [pegawai, setPegawai] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(8);
    const [pages, setPages] = useState(0);
    const [rows, setRows] = useState(0);
    const [keyword, setKeyword] = useState("");
    const [query, setQuery] = useState("");
    const [msg, setMsg] = useState("");
    const { user } = useSelector((state) => state.auth);


    useEffect(() => {
        getPegawai();
        document.title = 'Pegawai Apps | ASN';
    }, [page, keyword]);

    const getPegawai = async () => {
        const response = await axios.get(
            `http://localhost:5000/pegawaiAsn?search_query=${keyword}&page=${page}&limit=${limit}`
        );
        setPegawai(response.data.result);
        setPage(response.data.page);
        setPages(response.data.totalPage);
        setRows(response.data.totalRows);
    };

    const changePage = ({ selected }) => {
        setPage(selected);
        if (selected === 9) {
            setMsg("If you don't find the data you are looking for, please search for data with specific keywords!");
        } else {
            setMsg("");
        }
    };

    const searchData = (e) => {
        e.preventDefault();
        setPage(0);
        setKeyword(query);
    };

    const resetKeyword = () => {
        setKeyword("");
    };

    return (
        <LayoutHome>
            <div>
                <div className="container mt-5">
                    <div className="tags are-large">
                        <h1 className='title mt-2 tag is-info'><IoPeople className='mr-1' /> DATA PEGAWAI</h1>
                    </div>
                    <form onSubmit={searchData}>
                        <div className="field has-addons">
                            <div className="control is-expanded">
                                <input type="text" className="input" placeholder='Cari Pegawai...' value={query} onChange={(e) => setQuery(e.target.value)} />
                            </div>
                            <div className="control">
                                <button type='submit' className='button is-info'><IoSearch className='mr-1' /> Cari</button>
                                {
                                    keyword === '' ? '' : <button onClick={() => resetKeyword()} className='button is-default'><IoReloadOutline className='mr-1' /> Reset</button>
                                }
                            </div>
                        </div>
                    </form>

                    <div class="tabs is-centered">
                        <ul>
                            <ul>
                                <li className='is-active'><Link to="/">ASN</Link></li>
                                <li><Link to="/pegawai-non-asn">Non ASN</Link></li>
                            </ul>
                        </ul>
                    </div>

                    <div className="columns is-multiline mt-1">
                        {pegawai.map((peg) => (
                            <div className="column is-one-quarter" key={peg.uuid}>
                                <div className="card">
                                    <div className="card-image">
                                        <figure className="image is-6by4">
                                            <img src={peg.url} alt="Placeholder image" />
                                        </figure>
                                    </div>
                                    <div className="card-content">
                                        <div className="media">
                                            <div className="media-content">
                                                <p className="title is-5">{peg.name}</p>
                                                <p className="subtitle is-6">{peg.nip}</p>
                                            </div>
                                        </div>
                                        {
                                            user === null ?
                                                ""
                                                :
                                                <footer className="card-footer">
                                                    <Link to={`views/${peg.uuid}`} className='card-footer-item'><IoEyeOutline className='mr-1' /> Views</Link>
                                                </footer>
                                        }
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                    <p>
                        Total Rows: {rows} Page: {rows ? page + 1 : 0} of {pages}
                    </p>
                    <p className='has-text-centered has-text-danger'>{msg}</p>
                    <nav className="pagination is-centered mb-5" key={rows} role="navigation" aria-label='pagination'>
                        <ReactPaginate
                            previousLabel={"< Prev"}
                            nextLabel={"Next >"}
                            pageCount={Math.min(10, pages)}
                            onPageChange={changePage}
                            containerClassName={"pagination-list"}
                            pageLinkClassName={"pagination-link"}
                            previousLinkClassName={"pagination-previous"}
                            nextLinkClassName={"pagination-next"}
                            activeLinkClassName={"pagination-link is-current"}
                            disabledLinkClassName={"pagination-link is-disabled"}
                        />
                    </nav>
                </div>
            </div>
        </LayoutHome>
    )
}

export default PegawaiAsn