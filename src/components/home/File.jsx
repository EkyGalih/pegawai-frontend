import React, { useState, useEffect } from 'react'
import LayoutHome from '../../pages/LayoutHome'
import axios from 'axios'
import { IoDownload, IoFileTrayFullSharp, IoSearch } from 'react-icons/io5'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ReactPaginate from 'react-paginate'
import { Link, useNavigate } from 'react-router-dom'

const FileHome = () => {
    const [files, setFile] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(8);
    const [pages, setPages] = useState(0);
    const [rows, setRows] = useState(0);
    const [keyword, setKeyword] = useState("");
    const [query, setQuery] = useState("");
    const [msg, setMsg] = useState("");
    const [icon, setIcon] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        getFile();
        document.title = 'Pegawai Apps | Files';
    }, [page, keyword]);

    const getFile = async () => {
        const response = await axios.get(
            `http://localhost:5000/file?search_query=${keyword}&page=${page}&limit=${limit}`
        );
        setFile(response.data.result);
        setPage(response.data.page);
        setPages(response.data.totalPage);
        setRows(response.data.totalRows);
    };

    const changePage = ({ selected }) => {
        setPage(selected);
        if (selected === 9) {
            setMsg("If you don't find the data you are looking for, please search for data with specific keywords!");
        } else {
            setMsg("");
        }
    };

    const searchData = (e) => {
        e.preventDefault();
        setPage(0);
        setKeyword(query);
    };

    const downloadFile = (url) => {
        navigate(url);
    }

    return (
        <LayoutHome>
            <div className="container mt-5">
                <div className="tags are-large">
                    <h1 className='title mt-2 tag is-info'><IoFileTrayFullSharp className='mr-1' /> BERKAS</h1>
                </div>
                <form onSubmit={searchData}>
                    <div className="field has-addons">
                        <div className="control is-expanded">
                            <input type="text" className="input" placeholder='Cari File...' value={query} onChange={(e) => setQuery(e.target.value)} />
                        </div>
                        <div className="control">
                            <button type='submit' className='button is-info'><IoSearch className='mr-1' /> Cari</button>
                        </div>
                    </div>
                </form>
                <table className='table is-striped is-hoverable is-fullwidth'>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama File</th>
                            <th>Jenis File</th>
                            <th>Extension File</th>
                            <th>Uploaded By</th>
                            <th>From</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {files.map((file, i) => (
                            <tr key={file.uuid}>
                                <td>{i + 1}</td>
                                <td>{file.nama_files}</td>
                                <td>{file.jenis_file}</td>
                                <td>
                                    {file.ext_file}
                                </td>
                                <td>{file.pegawai.name}</td>
                                <td>{file.bidang.nama_bidang}</td>
                                <td>
                                    <button className='button is-info is-small' onClick={() => downloadFile(file.url)}>
                                        <IoDownload /> Download
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <p>
                    Total Rows: {rows} Page: {rows ? page + 1 : 0} of {pages}
                </p>
                <p className='has-text-centered has-text-danger'>{msg}</p>
                <nav className="pagination is-centered mb-5" key={rows} role="navigation" aria-label='pagination'>
                    <ReactPaginate
                        previousLabel={"< Prev"}
                        nextLabel={"Next >"}
                        pageCount={Math.min(10, pages)}
                        onPageChange={changePage}
                        containerClassName={"pagination-list"}
                        pageLinkClassName={"pagination-link"}
                        previousLinkClassName={"pagination-previous"}
                        nextLinkClassName={"pagination-next"}
                        activeLinkClassName={"pagination-link is-current"}
                        disabledLinkClassName={"pagination-link is-disabled"}
                    />
                </nav>
            </div>
        </LayoutHome>
    )
}

export default FileHome