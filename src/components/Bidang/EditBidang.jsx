import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate, useParams } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoCreateOutline, IoHome, IoPlayBack, IoSave } from 'react-icons/io5';

const EditBidang = () => {
    const [nama_bidang, setBidang] = useState("");
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getBidangById();
        document.title = 'Pegawai Apps | Edit Bidang';
    }, []);

    const getBidangById = async (e) => {
        const response = await axios.get(`http://localhost:5000/bidang/${id}`);
        setBidang(response.data.nama_bidang);
    }

    const updateBidang = async (e) => {
        e.preventDefault();
        try {
            await axios.patch(`http://localhost:5000/bidang/${id}`, {
                nama_bidang: nama_bidang
            });
            navigate("/bidang");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoHome/> Bidang</h1>
            <h2 className='subtitle'><IoCreateOutline/> Edit Bidang</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <form onSubmit={updateBidang}>
                        <div className="field">
                            <label className="label">Nama Bidang</label>
                            <div className="control">
                                <input type="text" className="input" value={nama_bidang} onChange={(e) => setBidang(e.target.value)} />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoSave/> Update</button>
                                <Link to="/bidang" className='button is-default'><IoPlayBack/> Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default EditBidang