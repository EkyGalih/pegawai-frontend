import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoAdd, IoHome, IoPlayBack } from 'react-icons/io5';

const AddBidang = () => {
    const [nama_bidang, setBidang] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        document.title = 'Pegawai Apps | Add Bidang';
    });

    const saveBidang = async (e) => {
        e.preventDefault();
        try {
            await axios.post("http://localhost:5000/bidang", {
                nama_bidang: nama_bidang
            });
            navigate("/bidang");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoHome/> Bidang</h1>
            <h2 className='subtitle'><IoAdd/> Add Bidang</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <form onSubmit={saveBidang}>
                        <div className="field">
                            <label className="label">Nama Bidang</label>
                            <div className="control">
                                <input type="text" className="input" value={nama_bidang} onChange={(e) => setBidang(e.target.value)} autoFocus />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoAdd/> Add</button>
                                <Link to="/bidang" className='button is-default'><IoPlayBack className='mr-1'/> Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default AddBidang