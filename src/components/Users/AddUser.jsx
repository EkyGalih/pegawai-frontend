import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import Layout from '../../pages/Layout'
import { IoAdd, IoPeopleOutline, IoPersonAddOutline, IoPlayBack } from 'react-icons/io5'
import { useDispatch, useSelector } from 'react-redux'
import { getMe } from '../../features/authSlice'

const AddUser = () => {
    const [pegawaiUuid, setPegawai] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [role, setRole] = useState("");
    const [confPassword, setPassword2] = useState("");
    const [msg, setMsg] = useState("");
    const [error, setError] = useState("");

    const [pegawais, setPegawais] = useState([]);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { isError } = useSelector((state => state.auth));

    useEffect(() => {
        dispatch(getMe());
        document.title = 'Pegawai Apps | Add User';
    }, [dispatch]);

    useEffect(() => {
        if (isError) {
            navigate("/");
        }
    }, [isError, navigate]);

    useEffect(() => {
        getPegawai();
    }, []);

    const getPegawai = async () => {
        const response = await axios.get("http://localhost:5000/pegawai");
        setPegawais(response.data.result);
    };

    const saveUser = async (e) => {
        e.preventDefault();
        try {
            await axios.post("http://localhost:5000/users", {
                username: username,
                email: email,
                password: password,
                confPassword: confPassword,
                role: role,
                pegawaiUuid: pegawaiUuid
            })
            navigate("/users");
        } catch (error) {
            if (error.response) {
                setMsg(error.response.data.msg);
                setError('notification is-danger');
            }
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPeopleOutline /> Users</h1>
            <h2 className='subtitle'><IoPersonAddOutline /> Add New User</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                <p className={error}>{msg}</p>
                    <form onSubmit={saveUser}>
                        <div className="field">
                            <label className="label">Pegawai</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={pegawaiUuid} onChange={(e) => setPegawai(e.target.value)}>
                                        <option value="">------</option>
                                        {pegawais.map((peg) => (
                                            <option value={peg.uuid}>{peg.name}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Username</label>
                            <div className="control">
                                <input type="text" className="input" placeholder='Username' value={username} onChange={(e) => setUsername(e.target.value)} />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">E-Mail</label>
                            <div className="control">
                                <input type="email" className="input" placeholder='E-Mail' value={email} onChange={(e) => setEmail(e.target.value)} />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Role</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={role} onChange={(e) => setRole(e.target.value)}>
                                        <option value="">--------</option>
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Password</label>
                            <div className="control">
                                <input type="password" className="input" placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)} />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Confirm Password</label>
                            <div className="control">
                                <input type="password" className="input" placeholder='Confirm Password' value={confPassword} onChange={(e) => setPassword2(e.target.value)} />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoAdd /> Add</button>
                                <Link to="/users" className='button is-default'><IoPlayBack className='mr-1' /> Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default AddUser