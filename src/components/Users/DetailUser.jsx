import React, { useState, useEffect } from 'react'
import { IoPlayBack } from 'react-icons/io5';
import { Link, useParams } from 'react-router-dom';
import Layout from '../../pages/Layout';
import axios from 'axios';

const DetailUser = () => {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [role, setRole] = useState("");
    const [namaPegawai, setNamaPegawai] = useState("");
    const [preview, setPreview] = useState("");
    const { id } = useParams();

    useEffect(() => {
        getUserById();
    }, []);

    const getUserById = async () => {
        const response = await axios.get(`http://localhost:5000/users/${id}`);
        setUsername(response.data.username);
        setEmail(response.data.email);
        setRole(response.data.role);
        setNamaPegawai(response.data.pegawai.name);
        setPreview(response.data.pegawai.url);
    }

    return (
        <Layout>
            <div className="container mt-5">
                <div className="columns is-centered">
                    <div className="column is-two-fifths">
                        <div className="card">
                            <div className="card-image">
                                <figure className="image is-2by3">
                                    <img src={preview} alt="Placeholder image" />
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <table className='table is-fullwidth'>
                            <tbody>
                                <tr>
                                    <td><strong>Username </strong>{username}</td>
                                    <td><strong>Nama Pegawai </strong>{namaPegawai}</td>
                                </tr>
                                <tr>
                                    <td><strong></strong>{email}</td>
                                    <td>Role User <strong>{role.toUpperCase()}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="field mt-5">
                            <Link to="/users" className='button is-block is-primary'><IoPlayBack className='mr-1' /> Kembali</Link>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default DetailUser