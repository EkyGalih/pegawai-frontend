import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import { Link } from 'react-router-dom'
import Layout from '../../pages/Layout'
import { IoPeopleOutline, IoAdd, IoCreateOutline, IoTrash, IoEyeOutline, IoList, IoSearch, IoReloadOutline } from 'react-icons/io5'

const UserList = () => {
    const [users, setUsers] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(5);
    const [pages, setPages] = useState(0);
    const [rows, setRows] = useState(0);
    const [keyword, setKeyword] = useState("");
    const [query, setQuery] = useState("");
    const [msg, setMsg] = useState("");

    useEffect(() => {
        getUsers();
        document.title = 'Pegawai Apps | Users';
    }, [page, keyword]);

    const getUsers = async () => {
        const response = await axios.get(
            `http://localhost:5000/users?search_query=${keyword}&page=${page}&limit=${limit}`
        );
        setUsers(response.data.result);
        setPage(response.data.page);
        setPages(response.data.totalPage);
        setRows(response.data.totalRows);
    };

    const changePage = ({ selected }) => {
        setPage(selected);
        if (selected === 9) {
            setMsg("If you don't find the data you are looking for, please search for data with specific keywords!");
        } else {
            setMsg("");
        }
    };

    const searchData = (e) => {
        e.preventDefault();
        setPage(0);
        setKeyword(query);
    };

    const deleteUser = async (userId) => {
        try {
            await axios.delete(`http://localhost:5000/users/${userId}`);
            getUsers();
        } catch (error) {
            console.log(error);
        }
    };

    const resetKeyword = () => {
        setKeyword("");
    };

    return (
        <Layout>
            <h1 className='title'><IoPeopleOutline /> Users</h1>
            <h2 className='subtitle'><IoList /> List of Users</h2>
            <div className="container mt-5">
                <Link to="add" className='button is-success mb-5'><IoAdd /> Add New</Link>
                <form onSubmit={searchData}>
                    <div className="field has-addons">
                        <div className="control is-expanded">
                            <input type="text" className="input" placeholder='Find something here...' value={query} onChange={(e) => setQuery(e.target.value)} />
                        </div>
                        <div className="control">
                            <button type='submit' className='button is-info'><IoSearch className='mr-1' /> Search</button>
                            {
                                keyword === '' ? '' : <button onClick={() => resetKeyword()} className='button is-default'><IoReloadOutline className='mr-1' /> Reset</button>
                            }
                        </div>
                    </div>
                </form>
                <div className="columns is-multiline mt-2">
                    {users.map((user) => (
                        <div className="column is-one-quarter" key={user.uuid}>
                            <div className="card">
                                <div className="card-image">
                                    <figure className="image is6by4">
                                        <img src={user.pegawai.url} alt="Image" />
                                    </figure>
                                </div>

                                <div className="card-content">
                                    <div className="media">
                                        <div className="media-content">
                                            <p className="title is-5">{user.pegawai.name}</p>
                                            <p className="subtitle is-6">{user.pegawai.nip}</p>
                                            <p className="subtitle is-6">{user.username}</p>
                                        </div>
                                    </div>
                                </div>

                                <footer className="card-footer">
                                    <Link to={`edit/${user.uuid}`} className='card-footer-item'><IoCreateOutline /> Edit</Link>
                                    <Link to={`view/${user.uuid}`} className='card-footer-item'><IoEyeOutline /> View</Link>
                                    <a onClick={() => deleteUser(user.uuid)} className='card-footer-item'><IoTrash /> Delete</a>
                                </footer>
                            </div>
                        </div>
                    ))}
                </div>
                <p>
                    Total Rows: {rows} Page: {rows ? page + 1 : 0} of {pages}
                </p>
                <p className='has-text-centered has-text-danger'>{msg}</p>
                <nav className="pagination is-centered" key={rows} role="navigation" aria-label='pagination'>
                    <ReactPaginate
                        previousLabel={"< Prev"}
                        nextLabel={"Next >"}
                        pageCount={Math.min(10, pages)}
                        onPageChange={changePage}
                        containerClassName={"pagination-list"}
                        pageLinkClassName={"pagination-link"}
                        previousLinkClassName={"pagination-previous"}
                        nextLinkClassName={"pagination-next"}
                        activeLinkClassName={"pagination-link is-current"}
                        disabledLinkClassName={"pagination-link is-disabled"}
                    />
                </nav>
            </div>
        </Layout>
    )
}

export default UserList