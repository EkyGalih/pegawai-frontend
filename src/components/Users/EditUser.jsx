import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Layout from '../../pages/Layout';
import { IoSaveOutline, IoPlayBack, IoPeople, IoCreateOutline } from "react-icons/io5";

const EditUser = () => {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confPassword, setConfPassword] = useState("");
    const [pegawaiUuid, setPegawai] = useState("");
    const [role, setRole] = useState("");
    const [msg, setMsg] = useState("");
    const [error, setError] = useState("");

    const [pegawais, setPegawais] = useState([]);

    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getPegawai();
        getUserById();
        document.title = 'Pegawai Apps | Edit User';
    }, []);

    const getUserById = async () => {
        const response = await axios.get(`http://localhost:5000/users/${id}`);
        setUsername(response.data.username);
        setEmail(response.data.email);
        setRole(response.data.role);
        setPegawai(response.data.pegawaiUuid);
    };

    const getPegawai = async () => {
        const response = await axios.get("http://localhost:5000/pegawais");
        setPegawais(response.data);
    };

    const updateUser = async (e) => {
        e.preventDefault();
        try {
            await axios.patch(`http://localhost:5000/users/${id}`, {
                username: username,
                email: email,
                password: password,
                confPassword: confPassword,
                pegawaiUuid: pegawaiUuid
            });
            navigate("/users");
        } catch (error) {
            if (error.response) {
                setMsg(error.response.data.msg);
                setError('notification is-danger');
            }
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoPeople /> Users</h1>
            <h2 className='subtitle'><IoCreateOutline /> Update User <strong>{username}</strong></h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <p className={error}>{msg}</p>
                    <form onSubmit={updateUser}>
                        <div className="field">
                            <label className="label">Nama Pegawai</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={pegawaiUuid} onChange={(e) => setPegawai(e.target.value)}>
                                        <option value="">------</option>
                                        {pegawais.map((peg) => (
                                            <option value={peg.uuid}>{peg.name}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Username</label>
                            <div className="control">
                                <input type="text" className="input" value={username} onChange={(e) => setUsername(e.target.value)} placeholder="Username" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Email</label>
                            <div className="control">
                                <input type="text" className="input" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Role</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select value={role} onChange={(e) => setRole(e.target.value)}>
                                        <option value="">--------</option>
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Password</label>
                            <div className="control">
                                <input type="text" className="input" placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)} />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Conf Password</label>
                            <div className="control">
                                <input type="text" className="input" placeholder='Conf Password' value={confPassword} onChange={(e) => setConfPassword(e.target.value)} />
                            </div>
                        </div>

                        <div className="field">
                            <div className="control">
                                <button type="submit" className="button is-success mr-2">
                                    <IoSaveOutline /> Update
                                </button>
                                <Link to="/users" className='button is-default'><IoPlayBack className='mr-1' /> Back</Link>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default EditUser