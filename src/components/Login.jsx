import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { LoginUser, getMe, reset } from "../features/authSlice";
import { IoLockClosed, IoLogInOutline, IoPlayBack } from 'react-icons/io5';

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { user, isError, isSuccess, isLoading, message } = useSelector(
        (state) => state.auth
    );

    useEffect(() => {
        document.title = 'Pegawai Apps | Login';
        dispatch(getMe());
    }, [dispatch]);

    useEffect(() => {
        if (user || isSuccess) {
            if (user.role === 'admin') {
                // console.log(user.role);
                navigate("/dashboard");
            } else if (user.role === 'user') {
                // console.log(user.role);
                navigate("/");
            }
        }
        dispatch(reset());
    }, [user, isSuccess, dispatch, navigate]);

    const Auth = (e) => {
        e.preventDefault();
        dispatch(LoginUser({ email, password }));
    }

    return (
        <section className="hero has-background-gre-light is-fullheight is-fullwidth">
            <div className="hero-body">
                <div className="container">
                    <div className="columns is-centered">
                        <div className="column is-4">
                            <form onSubmit={Auth} className='box'>
                                {isError && <p className='has-text-centered'>{message}</p>}
                                <h1 className='title is-2'><IoLockClosed /> Sign In</h1>
                                <div className="field">
                                    <label className="label">Email</label>
                                    <div className="control">
                                        <input type="text" className="input" placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)} />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Password</label>
                                    <div className="control">
                                        <input type="password" className="input" placeholder='*****' value={password} onChange={(e) => setPassword(e.target.value)} />
                                    </div>
                                </div>
                                <div className="field mt-5">
                                    <button type='submit' className='button is-success is-fullwidth'>
                                        <IoLogInOutline className='mr-1' />{isLoading ? 'Loading...' : 'Login'}
                                    </button>
                                    <Link to="/" className='button is-default is-fullwidth mt-1'><IoPlayBack className='mr-1' /> Back</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login