import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoAdd, IoInfinite, IoPlayBack } from 'react-icons/io5';

const AddGolongan = () => {
    const [nama_golongan, setGolongan] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        document.title = 'Pegawai Apps | Add Golongan';
    });

    const saveGolongan = async (e) => {
        e.preventDefault();
        try {
            await axios.post("http://localhost:5000/golongan", {
                nama_golongan: nama_golongan
            });
            navigate("/golongan");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoInfinite/> Golongan</h1>
            <h2 className='subtitle'><IoAdd/> Add New Golongan</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <form onSubmit={saveGolongan}>
                        <div className="field">
                            <label className="label">Golongan</label>
                            <div className="control">
                                <input type="text" className="input" value={nama_golongan} onChange={(e) => setGolongan(e.target.value)} placeholder="Nama Golongan" autoFocus />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoAdd/> Add</button>
                                <Link to="/golongan" className='button is-default'><IoPlayBack className='mr-1'/> Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default AddGolongan