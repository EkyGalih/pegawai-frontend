import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate, useParams } from 'react-router-dom'
import Layout from '../../pages/Layout';
import { IoCreateOutline, IoInfinite, IoPlayBack, IoSave } from 'react-icons/io5';

const EditGolongan = () => {
    const [nama_golongan, setGolongan] = useState("");
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getGolonganById();
        document.title = 'Pegawai Apps | Edit Golongan';
    }, []);

    const getGolonganById = async () => {
        const response = await axios.get(`http://localhost:5000/golongan/${id}`);
        setGolongan(response.data.nama_golongan);
    }

    const updateGolongan = async (e) => {
        e.preventDefault();
        try {
            await axios.patch(`http://localhost:5000/golongan/${id}`, {
                nama_golongan: nama_golongan
            });
            navigate("/golongan");
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Layout>
            <h1 className='title'><IoInfinite/> Golongan</h1>
            <h2 className='subtitle'><IoCreateOutline/> Edit Golongan</h2>
            <div className="columns is-centered mt-5">
                <div className="column is-half">
                    <form onSubmit={updateGolongan}>
                        <div className="field">
                            <label className="label">Golongan</label>
                            <div className="control">
                                <input type="text" className="input" value={nama_golongan} onChange={(e) => setGolongan(e.target.value)} placeholder="Nama Golongan" />
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <button type='submit' className='button is-success mr-2'><IoSave/> Update</button>
                                <Link to="/golongan" className='button is-default'><IoPlayBack className='mr-1'/>Back</Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default EditGolongan